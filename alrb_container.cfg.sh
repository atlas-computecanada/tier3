#
#  This is a common script for all CC sites that configures containers
#    site specific changes should go into the sub-dirs defined by $CC_CLUSTER
#

if [ -d /project ]; then
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS '-B /project:/project'
elif [ -e $HOME/projects ]; then
    alrb_tmpAr=( `\find $HOME/projects -type l -exec readlink -f {} \; | \cut -d "/" -f -2 | \sort -u` )
    for alrb_tmp in ${alrb_tmpAr[@]}; do
	set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B $alrb_tmp:$alrb_tmp"
    done
fi

unset TMPDIR  # so that /scratch will not be used by TMPDIR
if [ -d /scratch ]; then
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS '-B /scratch:/scratch'
elif [ -l $HOME/scratch ]; then
    alrb_tmp=`readlink -f $HOME/scratch | cut -d "/" -f -2`
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B $alrb_tmp:$alrb_tmp"
fi

if [ -d /wlcg ]; then
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B /wlcg:/wlcg"
fi

set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B $ATLAS_LOCAL_ROOT_BASE/utilities/setupAliases.sh:/alrb/setupAliases.sh"

set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "source /alrb/setupAliases.sh"
set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export CC_CLUSTER='$CC_CLUSTER'"
set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export CC_AtlasSiteRoot='$CC_AtlasSiteRoot'"

set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "insertPath PATH $CC_AtlasSiteRoot/bin"

# setup the configurations that users have in $HOME, if any
run_ALRB_UserContainerConfig

# SLURM_TMPDIR defined in batch and salloc and is unique to job	    
# Do this last prior to starting the container since we need to 	    
#  save and restore settings to avoid propagating stale SLURM_TMPDIR
if [ ! -z $SLURM_TMPDIR ]; then
    # we always have ALRB_CONT_CMDOPTS at CC sites
    CC_ALRB_CONT_CMDOPTS="export ALRB_CONT_CMDOPTS='$ALRB_CONT_CMDOPTS'"
    if [ -z "$ALRB_CONT_PRESETUP" ]; then
	CC_ALRB_CONT_PRESETUP='unset ALRB_CONT_PRESETUP'
    else
	CC_ALRB_CONT_PRESETUP="export ALRB_CONT_PRESETUP='$ALRB_CONT_PRESETUP'"
    fi

    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B $SLURM_TMPDIR:$SLURM_TMPDIR"
    
    set_ALRB_ContainerEnv ALRB_CONT_PRESETUP "export SLURM_TMPDIR=$SLURM_TMPDIR;$CC_ALRB_CONT_CMDOPTS;$CC_ALRB_CONT_PRESETUP"
    unset CC_ALRB_CONT_CMDOPTS CC_ALRB_CONT_PRESETUP

fi

