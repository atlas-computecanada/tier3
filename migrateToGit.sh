#! /bin/bash
#!----------------------------------------------------------------------------
#!
#!  migrateToGit.sh 
#!
#!  Migrate /project/atlas/Tier3 to use the git repo
#!
#!  Usage:
#!    migrateToGit.sh
#!
#!  History:
#!    11Jan2019: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ ! -e AtlasUserSiteSetup.sh ]; then
    \echo "Error: You need to be in the /projetc/atlas/Tier3 dir to run"
    exit 64
fi

git --version

git init
if [ $? -ne 0 ]; then
    exit 64
fi

git remote add origin https://gitlab.cern.ch:8443/atlas-computecanada/tier3.git
if [ $? -ne 0 ]; then
    exit 64
fi

git fetch origin
if [ $? -ne 0 ]; then
    exit 64
fi

git reset --hard origin/master
if [ $? -ne 0 ]; then
    exit 64
fi

git branch --set-upstream master origin/master
if [ $? -ne 0 ]; then
    \echo "Ignore the above error ... will retry with new syntax ..."
    git branch --set-upstream-to=origin/master
    if [ $? -ne 0 ]; then
	exit 64
    fi
fi

\echo "Now run update to be sure ..."
./updateSW.sh

exit 0


