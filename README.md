# Tier3

This package contains scripts for ATLAS setups on Compute Canada sites for Tier3 users.

## New Installations:
```
git clone https://gitlab.cern.ch:8443/atlas-computecanada/tier3.git /project/atlas/Tier3
```

## Update: (This can be done as a cron job)
```
cd /project/atlas/Tier3
./updateSW.sh
```

## Developers:

### (first time) check out repo in your private dir
```
cd ~/dev
git clone ssh://git@gitlab.cern.ch:7999/atlas-computecanada/tier3.git Tier3
cd Tier3
```

### Make changes and commit
```
cd ~/dev/Tier3
git pull
(make changes)
(edit RELEASE.NOTES and write there also a tag of the form 
  V00-XX-YY where XX is a major change and YY is incremental)
git status
git commit -am "describe your changes"
git status
git push
```

### To Put into Production
```
cd ~/dev/Tier3
git pull
(edit productionVersion to show tag in RELEASE.NOTES)
git status
git commit -am "production version tag change"
git status
git push
git tag V00-XX-YY
git origin push V00-XX-YY
```




  



