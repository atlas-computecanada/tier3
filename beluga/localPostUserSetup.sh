#
# This is a local post setup script
#   You can, for example, put in a notice for users such as the example below
#

# for change in slurm that broke --export=NONE
alias sbatch='sbatch --export=CC_CLUSTER,CC_AtlasSiteRoot=$CC_AtlasSiteRoot'

# motd at the end
if [ -e $ALRB_localConfigDir/motd ]; then
    $ALRB_localConfigDir/motd
fi
