#
#  This is a comon script for all CC sites
#    site specific changes should go into the sub-dirs defined by $CC_CLUSTER
#

if [ ! `typeset  -f setupATLAS > /dev/null` ]; then
    unalias setupATLAS  > /dev/null 2>&1
    export CC_AtlasSiteRoot="`dirname ${BASH_SOURCE:-$0}`"
    export CC_AtlasSiteDir="$CC_AtlasSiteRoot/$CC_CLUSTER"
    if [ ! -d $CC_AtlasSiteDir ]; then
	\echo "Error: $CC_CLUSTER site is not ready to welcome ATLAS users"
	return 64
    fi
    function setupATLAS
    {    
	if [ -d  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]; then

	    if [ -e $CC_AtlasSiteDir/AtlasUserSiteSetup-site.sh ]; then
		source $CC_AtlasSiteDir/AtlasUserSiteSetup-site.sh
	    fi
	    export ALRB_localConfigDir=$CC_AtlasSiteDir
	    
	    export ALRB_containerSiteOnly=YES

            export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    	    
            source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
            return $?
	else
            \echo "Error: cvmfs atlas repo is unavailable"
            return 64
	fi
    }
fi
