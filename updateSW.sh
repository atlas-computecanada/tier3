#! /bin/bash
#!----------------------------------------------------------------------------
#!
#!  updateSW.sh
#!
#!  Script to update the software in /project/atlas/Tier3
#!
#!  Usage:
#!    updateSW.sh
#!
#!  History:
#!    11Jan2019: A. De Silva, first version.
#!
#!----------------------------------------------------------------------------

if [ ! -e ./productionVersion ]; then
    \echo "Error: You need to run this from the dir containing the CC ATLAS Tier3 sw"
# we do not cd automaically in the script to be safe and to allow us to test 
#  this in another dir
    exit 64
fi

git fetch origin
if [ $? -ne 0 ]; then
    exit 64
fi	

git reset --hard origin/master
if [ $? -ne 0 ]; then
    exit 64
fi

atlascc_swVersion=`\cat productionVersion`

git checkout tags/$atlascc_swVersion
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "Successful completion of update"

exit 0


